	__nest__ (
		__all__,
		'scalesgenerator', {
			__all__: {
				__inited__: false,
				__init__: function (__all__) {
					var random = {};
					var __name__ = 'scalesgenerator';
					print ('importing...');
					__nest__ (random, '', __init__ (__world__.random));
					var py_keys = list ([]);
					var scales_generator = __class__ ('scales_generator', [object], {
						__module__: __name__,
						get __init__ () {return __get__ (this, function (self, py_keys, contrary_motion, cromatic, hands, broken_cords, scale_tempo, arpeggio_tempo, grade) {
							if (typeof py_keys == 'undefined' || (py_keys != null && py_keys .hasOwnProperty ("__kwargtrans__"))) {;
								var py_keys = list (['B major', 'Bb major', 'f minor', 'Eb major', 'c minor', 'Ab major', 'g# minor', 'Db major', 'c# minor']);
							};
							if (typeof contrary_motion == 'undefined' || (contrary_motion != null && contrary_motion .hasOwnProperty ("__kwargtrans__"))) {;
								var contrary_motion = list (['F major', 'Eb major', 'd minor', 'c minor']);
							};
							if (typeof cromatic == 'undefined' || (cromatic != null && cromatic .hasOwnProperty ("__kwargtrans__"))) {;
								var cromatic = list (['C#/Db', 'D#/Eb', 'F#/Gb', 'G#/Ab', 'A#/Bb']);
							};
							if (typeof hands == 'undefined' || (hands != null && hands .hasOwnProperty ("__kwargtrans__"))) {;
								var hands = list ([' LH', ' RH', ' HT']);
							};
							if (typeof broken_cords == 'undefined' || (broken_cords != null && broken_cords .hasOwnProperty ("__kwargtrans__"))) {;
								var broken_cords = list ([]);
							};
							if (typeof scale_tempo == 'undefined' || (scale_tempo != null && scale_tempo .hasOwnProperty ("__kwargtrans__"))) {;
								var scale_tempo = '104bpm';
							};
							if (typeof arpeggio_tempo == 'undefined' || (arpeggio_tempo != null && arpeggio_tempo .hasOwnProperty ("__kwargtrans__"))) {;
								var arpeggio_tempo = '76bpm';
							};
							if (typeof grade == 'undefined' || (grade != null && grade .hasOwnProperty ("__kwargtrans__"))) {;
								var grade = '4';
							};
							print ('making key and starting points...');
							self.scales = list ([]);
							self.py_keys = py_keys;
							self.contrary_motion = contrary_motion;
							self.cromatic = cromatic;
							self.hands = hands;
							self.base_hands = self.hands.__getslice__ (0, null, 1);
							self.broken_cords = broken_cords;
							self.scale_tempo = scale_tempo;
							self.arpeggio_tempo = arpeggio_tempo;
							self.grade = grade;
						});},
						get generate_scales () {return __get__ (this, function (self) {
							print ('generating scales...');
							if (len (self.py_keys) != 0) {
								var __iterable0__ = self.hands;
								for (var __index0__ = 0; __index0__ < len (__iterable0__); __index0__++) {
									var hand = __iterable0__ [__index0__];
									var key = self.py_keys.py_pop (random.randint (0, len (self.py_keys) - 1));
									var scale = (key + ' scale') + hand;
									self.scales.append (scale);
								}
							}
							self.hands = self.base_hands.__getslice__ (0, null, 1);
						});},
						get generate_arpeggios () {return __get__ (this, function (self) {
							print ('generating arpeggios...');
							if (len (self.py_keys) != 0) {
								var __iterable0__ = self.hands;
								for (var __index0__ = 0; __index0__ < len (__iterable0__); __index0__++) {
									var hand = __iterable0__ [__index0__];
									var key = self.py_keys.py_pop (random.randint (0, len (self.py_keys) - 1));
									var arpegio = (key + ' arpeggio') + hand;
									self.scales.append (arpegio);
								}
								self.hands = self.base_hands.__getslice__ (0, null, 1);
							}
						});},
						get generate_contrary_motion () {return __get__ (this, function (self) {
							print ('generating contrary motion...');
							if (len (self.contrary_motion) != 0) {
								var contrary = self.contrary_motion.py_pop (random.randint (0, len (self.contrary_motion) - 1)) + ' contrary motion';
								self.scales.append (contrary);
							}
						});},
						get generate_broken_cords () {return __get__ (this, function (self) {
							print ('generating broken cords...');
							if (len (self.broken_cords) != 0) {
								var broken_cord = self.broken_cords.py_pop (random.randint (0, len (self.broken_cords) - 1));
								self.scales.append (broken_cord);
							}
						});},
						get generate_cromatic () {return __get__ (this, function (self) {
							print ('generating cromatic...');
							if (len (self.cromatic) != 0) {
								var Cromatic_options = list ([]);
								var __iterable0__ = self.hands;
								for (var __index0__ = 0; __index0__ < len (__iterable0__); __index0__++) {
									var hand = __iterable0__ [__index0__];
									var Cromatic = (self.cromatic.py_pop (random.randint (0, len (self.cromatic) - 1)) + ' cromatic') + hand;
									Cromatic_options.append (Cromatic);
								}
								random.shuffle (Cromatic_options);
								self.scales.append (Cromatic_options.py_pop ());
							}
						});},
						get generator () {return __get__ (this, function (self) {
							self.generate_arpeggios ();
							self.generate_broken_cords ();
							self.generate_contrary_motion ();
							self.generate_cromatic ();
							self.generate_scales ();
							print ('\n');
							print (('ABRSM Grade ' + self.grade) + ' scales and arpeggios (semi-random selection)');
							random.shuffle (self.scales);
							print ('Remember to play your scales smothly and calmly.');
							print ((('Scales tempo:' + self.scale_tempo) + '\nArpeggio tempo:') + self.arpeggio_tempo);
							print ('Enjoy!\nCredits:\n  Website: Michel Newton\n  Logic: Stefan Newton\n  Requierments: ABRSM');
							var prelude = ('ABRSM Grade ' + self.grade) + ' scales and arpeggios (semi-random selection)';
							var end_strings = list (['Remember to play your scales smothly and calmly', 'Scales tempo:' + self.scale_tempo, 'Arpeggio tempo:' + self.arpeggio_tempo, 'Enjoy!', 'Note: this is only a guide', 'Credits:', '  Website: Michel Newton', '  Logic: Stefan Newton', '  Requierments: ABRSM']);
							self.scales.insert (0, prelude);
							var __iterable0__ = end_strings;
							for (var __index0__ = 0; __index0__ < len (__iterable0__); __index0__++) {
								var end = __iterable0__ [__index0__];
								self.scales.append (end);
							}
							return self.scales;
						});}
					});
					var Grade_control = function (grade) {
						if (typeof grade == 'undefined' || (grade != null && grade .hasOwnProperty ("__kwargtrans__"))) {;
							var grade = 4;
						};
						if (grade == 1) {
							var n = scales_generator (__kwargtrans__ ({py_keys: list (['C major', 'G major', 'D major', 'F major', 'A minor', 'D minor']), Arpeggios: list ([]), contrary_motion: list (['C major']), cromatic: list ([]), hands: list (['RH', 'LH']), broken_cords: list (['C major', 'G major', 'F major', 'A minor', 'D minor']), scale_tempo: '60bpm', arpeggio_tempo: '46bpm', grade: '1'}));
							var scales = n.generator ();
							return scales;
						}
						else if (grade == 2) {
							var n = scales_generator (__kwargtrans__ ({py_keys: list (['A major', 'G major', 'D major', 'F major', 'E minor', 'D minor', 'G minor']), Arpeggios: list (['G major', 'D major', 'A major', 'D minor', 'G minor']), contrary_motion: list (['C major', 'E major']), cromatic: list (['D']), hands: list (['RH', 'LH', 'HT']), broken_cords: list (['F major', 'E minor']), scale_tempo: '66bpm', arpeggio_tempo: '63bpm', grade: '2'}));
							var scales = n.generator ();
							return scales;
						}
						else if (grade == 3) {
							var n = scales_generator (__kwargtrans__ ({py_keys: list (['A major', 'E major', 'B major', 'Bb major', 'Eb major', 'B minor', 'G minor', 'C minor']), Arpeggios: list (['A major', 'G minor', 'E major', 'B major', 'Bb major', 'Eb major', 'B minor', 'C minor']), contrary_motion: list (['A major', 'A minor']), cromatic: list (['Ab/G#', 'C']), hands: list (['RH', 'LH', 'HT']), broken_cords: list ([]), scale_tempo: '80bpm', arpeggio_tempo: '69bpm', grade: '3'}));
							var scales = n.generator ();
							return scales;
						}
						else if (grade == 4) {
							var n = scales_generator ();
							var scales = n.generator ();
							return scales;
						}
					};
					__pragma__ ('<use>' +
						'random' +
					'</use>')
					__pragma__ ('<all>')
						__all__.Grade_control = Grade_control;
						__all__.__name__ = __name__;
						__all__.py_keys = py_keys;
						__all__.scales_generator = scales_generator;
					__pragma__ ('</all>')
				}
			}
		}
	);
