print('importing...')
import random
keys = []
class scales_generator:
    def __init__(self,keys=['B major','Bb major','f minor','Eb major','c minor','Ab major','g# minor','Db major','c# minor'],contrary_motion=['F major','Eb major','d minor','c minor'],cromatic=['C#/Db','D#/Eb','F#/Gb','G#/Ab','A#/Bb'],hands=[' LH',' RH',' HT'],broken_cords=[],scale_tempo='104bpm',arpeggio_tempo='76bpm',grade='4'):
        print('making key and starting points...')
        self.scales = []
        self.keys = keys
        self.contrary_motion = contrary_motion
        self.cromatic = cromatic
        self.hands = hands
        self.base_hands = self.hands[:]
        self.broken_cords = broken_cords
        self.scale_tempo = scale_tempo
        self.arpeggio_tempo = arpeggio_tempo
        self.grade = grade

    def generate_scales(self):
        print('generating scales...')
        if len(self.keys) != 0:
            for hand in self.hands:
                key = self.keys.pop(random.randint(0, (len(self.keys)-1)))
                scale = key + ' scale' + hand
                self.scales.append(scale)
        self.hands = self.base_hands[:]

    def generate_arpeggios(self):
        print('generating arpeggios...')
        if len(self.keys) != 0:
            for hand in self.hands:
                key = self.keys.pop(random.randint(0, (len(self.keys)-1)))
                arpegio = key + ' arpeggio' + hand
                self.scales.append(arpegio)
            self.hands = self.base_hands[:]

    def generate_contrary_motion(self):
        print('generating contrary motion...')
        if len(self.contrary_motion) != 0:
            contrary = self.contrary_motion.pop(random.randint(0, (len(self.contrary_motion)-1))) + ' contrary motion'
            self.scales.append(contrary)

    def generate_broken_cords(self):
        print('generating broken cords...')
        if len(self.broken_cords) != 0:
            broken_cord = self.broken_cords.pop(random.randint(0, (len(self.broken_cords)-1)))
            self.scales.append(broken_cord)

    def generate_cromatic(self):
        print('generating cromatic...')
        if len(self.cromatic) != 0:
            Cromatic_options = []
            for hand in self.hands:
                Cromatic = self.cromatic.pop(random.randint(0, (len(self.cromatic)-1))) + ' cromatic' + hand
                Cromatic_options.append(Cromatic)
            random.shuffle(Cromatic_options)
            self.scales.append(Cromatic_options.pop())

    def generator(self):
        self.generate_arpeggios()
        self.generate_broken_cords()
        self.generate_contrary_motion()
        self.generate_cromatic()
        self.generate_scales()
        print('\n')
        print('ABRSM Grade '+self.grade+' scales and arpeggios (semi-random selection)')
        random.shuffle(self.scales)
        print('Remember to play your scales smothly and calmly.')
        print('Scales tempo:'+self.scale_tempo+'\nArpeggio tempo:'+self.arpeggio_tempo)
        # credits
        print('Enjoy!\nCredits:\n  Website: Michel Newton\n  Logic: Stefan Newton\n  Requierments: ABRSM')
        prelude = 'ABRSM Grade ' + self.grade + ' scales and arpeggios (semi-random selection)'
        end_strings = [
            'Remember to play your scales smothly and calmly',
            'Scales tempo:' + self.scale_tempo,
            'Arpeggio tempo:' +self.arpeggio_tempo,
            'Enjoy!',
            'Note: this is only a guide','Credits:',
            '  Website: Michel Newton',
            '  Logic: Stefan Newton',
            '  Requierments: ABRSM' ]

        self.scales.insert(0, prelude)
        for end in end_strings:
            self.scales.append(end)

        return self.scales

def Grade_control(grade=4):
    if grade == 1:
        n = scales_generator(keys=['C major','G major','D major','F major','A minor','D minor'], Arpeggios=[], contrary_motion=['C major'],cromatic=[],hands=['RH','LH'],broken_cords=['C major','G major','F major','A minor','D minor'],scale_tempo='60bpm',arpeggio_tempo='46bpm',grade='1')
        scales =  n.generator()
        return scales
    elif grade == 2:
        n = scales_generator(keys=['A major','G major','D major','F major','E minor','D minor','G minor'], Arpeggios=['G major','D major','A major','D minor','G minor'], contrary_motion=['C major','E major'],cromatic=['D'],hands=['RH','LH','HT'],broken_cords=['F major','E minor'],scale_tempo='66bpm',arpeggio_tempo='63bpm',grade='2')
        scales = n.generator()
        return scales
    elif grade == 3:
        n = scales_generator(keys=['A major','E major','B major','Bb major','Eb major','B minor','G minor','C minor'], Arpeggios=['A major','G minor','E major','B major','Bb major','Eb major','B minor','C minor'], contrary_motion=['A major','A minor'],cromatic=['Ab/G#','C'],hands=['RH','LH','HT'],broken_cords=[],scale_tempo='80bpm',arpeggio_tempo='69bpm',grade='3')
        scales = n.generator()
        return scales
    elif grade == 4:
        n = scales_generator()
        scales = n.generator()
        return scales



